package com.vizorgames.interview.ioc;

import com.vizorgames.interview.exception.BindingNotFoundException;
import com.vizorgames.interview.exception.ConstructorAmbiguityException;
import com.vizorgames.interview.exception.NoSuitableConstructorException;

import javax.inject.Inject;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

public class InjectorImpl implements Injector {

    private Map<Class, Provider> providers = new ConcurrentHashMap<>();
    private Set<Class> dirty = ConcurrentHashMap.newKeySet();

    @Override
    public <T> Provider<T> getProvider(Class<T> type) {
        Provider provider = providers.get(type);
        if (provider != null) {
            dirty.add(type);
            provider.init();
        }
        return provider;
    }

    @Override
    public <T> void bind(Class<T> base, Class<? extends T> impl) {
        Provider<? extends T> eagerProvider = createEagerProvider(impl);
        providers.compute(base, compute(impl, eagerProvider));
    }

    private <T> BiFunction<Class, Provider, Provider> compute(Class<? extends T> impl, Provider<? extends T> eagerProvider) {
        return (k, v) -> {
            if (dirty.contains(impl)) {
                throw new RuntimeException("Too late for bind");
            } else {
                return eagerProvider;
            }
        };
    }

    private <T> Provider<T> createEagerProvider(Class<T> impl) {
        return new EagerProvider<>(impl, providers);
    }

    @Override
    public <T> void bindSingleton(Class<T> base, Class<? extends T> impl) {
        Provider<? extends T> lazyProvider = createLazyProvider(impl);
        providers.compute(base, compute(impl, lazyProvider));
    }

    private <T> Provider<T> createLazyProvider(Class<T> impl) {
        return new LazyProvider<>(impl, providers);
    }

}

class EagerProvider<T> implements Provider<T> {

    private final Class<T> impl;
    private final Map<Class, Provider> providers;

    public EagerProvider(Class<T> impl, Map<Class, Provider> providers) {
        this.impl = impl;
        this.providers = providers;
    }

    @Override
    public T getInstance() {
        return InjectionUtils.get(impl, providers);
    }

    @Override
    public void init() {
        InjectionUtils.get(impl, providers);
    }
}

class LazyProvider<T> implements Provider<T> {

    private final Class<T> impl;
    private final Map<Class, Provider> providers;
    private volatile T t;

    public LazyProvider(Class<T> impl, Map<Class, Provider> providers) {
        this.impl = impl;
        this.providers = providers;
    }

    @Override
    public T getInstance() {
        if (t == null) {
            synchronized (this) {
                if (t == null) {
                    t = InjectionUtils.get(impl, providers);
                }
            }
        }
        return t;
    }

    @Override
    public void init() {
    }

}

class InjectionUtils {

    private InjectionUtils() {
    }

    public static <T> T get(Class<T> impl, Map<Class, Provider> providers) {
        T t = null;
        try {
            Constructor<?>[] constructors = impl.getConstructors();
            Constructor<?> defaultConstructor = null;
            Constructor<?> lastWithInject = null;
            int injectCount = 0;
            for (Constructor<?> c : constructors) {

                if (c.getParameterCount() == 0) {
                    defaultConstructor = c;
                }

                if (c.getAnnotation(Inject.class) != null) {
                    injectCount++;
                    lastWithInject = c;
                    if (injectCount > 1) {
                        throw new ConstructorAmbiguityException();
                    }
                }
            }

            if (injectCount == 0) {
                if (defaultConstructor == null) {
                    throw new NoSuitableConstructorException();
                } else {
                    t = impl.getDeclaredConstructor().newInstance();
                }
            } else if (injectCount == 1) {
                Class<?>[] parameterTypes = lastWithInject.getParameterTypes();
                Object[] constructorArgs = new Object[parameterTypes.length];
                for (int i = 0; i < parameterTypes.length; i++) {
                    Provider provider = providers.get(parameterTypes[i]);
                    if (provider == null) {
                        throw new BindingNotFoundException();
                    }
                    constructorArgs[i] = provider.getInstance();
                }
                t = impl.getDeclaredConstructor(parameterTypes).newInstance(constructorArgs);
            }

        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return t;
    }
}
