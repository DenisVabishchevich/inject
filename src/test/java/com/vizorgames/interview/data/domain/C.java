package com.vizorgames.interview.data.domain;

import javax.inject.Inject;

public class C {
    public B b;

    @Inject
    public C(B b) {
        this.b = b;
    }
}