package com.vizorgames.interview.data.domain;

import javax.inject.Inject;

public class B {
    public A a;

    @Inject
    public B(A a) {
        this.a = a;
    }
}
